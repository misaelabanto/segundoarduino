#ifndef HELPERS_H_
#define HELPERS_H_

#include "Arduino.h"
#include "http.h"

#define PIN_TEMPERATURE A1
#define PIN_LIGHT A2

void postTemperature();
void postLight();

#endif
