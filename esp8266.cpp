#include "esp8266.h"
SoftwareSerial esp8266 (10, 11);

String atCommand(String command, int timeout) {
  String response = "";
  esp8266.println(command);

  long int time = millis();

  while( (time+timeout) > millis() ) {
    while(esp8266.available()) {
      char tmp = esp8266.read();
      response += tmp;
    }
  }
  Serial.println(response);
  return response;
}

void connectToWifi() {
  String connection = "AT+CWJAP=\"" +ssid+"\",\"" + password + "\"";
  atCommand(connection, 6000);
  atCommand("AT+CIFSR", timeout);
}

void setupESP8266() {
  esp8266.begin(9600);
  atCommand("AT+RST", timeout);
  atCommand("AT+CWMODE=1", timeout);
}

void startTCPConnection() {
  String connection = "AT+CIPSTART=\"TCP\",\"" + server + "\",80";
  atCommand(connection, timeout); 
}

void closeTCPConnection() {
  atCommand("AT+CIPCLOSE", timeout);
}

String sendRequest(String request) {
  atCommand("AT+CIPSEND=" + String(request.length()) , timeout);
  String response = atCommand(request, 6000);
  return response;
}

String buildRequest(String method, String path) {
  bool esPost = method.equals("POST");
  String request = esPost ? "POST " : "GET ";
  request += path;
  request += String(" HTTP/1.1\r\n") + 
      String("Host: ") + server + String("\r\n") + 
      String("Connection: keep-alive\r\n");
  if(esPost) {
    request += "Content-Type: application/json\r\n";
    request += "Content-Length: ";
  } else {
    request += "\r\n";
  }
  Serial.println(request);
  return request;
}

String buildPost(String path, String data){
  String requestPost = buildRequest("POST", path);
  requestPost += String(data.length());
  requestPost += "\r\n\r\n";
  requestPost += data;
  return requestPost;
}

String buildGet(String path){
  return buildRequest("GET", path);
}
