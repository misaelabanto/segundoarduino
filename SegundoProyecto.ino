#include "helpers.h"

void setup(){
  Serial.begin(9600);
  setupWifi();
}

void loop(){
  postTemperature();
  postLight();
  delay(1000);
}
