#ifndef ESP8266_H_
#define ESP8266_H_
#include <SoftwareSerial.h>
#include "Arduino.h"

const String server = "smart-house-uni.herokuapp.com";
const int timeout = 2000;
const String ssid = "27737474729293747373892937473";
const String password = "unodostres123";

String atCommand(String command, int timeout);
void setupESP8266();
void connectToWifi();
void startTCPConnection();
void closeTCPConnection();
String sendRequest(String request);
String buildRequest(String method, String path);
String buildPost(String path, String data);
String buildGet(String path);

#endif
