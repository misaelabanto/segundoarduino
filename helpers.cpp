#include "helpers.h"

void postTemperature(){
  int temperature = analogRead(PIN_TEMPERATURE);
  sendPost("/sensor", "{\"type\": \"temperatura\",\"value\": " + String(temperature) + "}");
}

void postLight(){
  int light = analogRead(PIN_LIGHT);
  sendPost("/sensor", "{\"type\": \"luminosidad\",\"value\": " + String(light) + "}");
}
