#ifndef HTTP_H_
#define HTTP_H_
#include "Arduino.h"
#include "esp8266.h"

void setupWifi();
String sendPost(String path, String data);
String sendGet(String path);

#endif
