#include "http.h"

void setupWifi(){
  setupESP8266();
  connectToWifi();
}
String sendPost(String path, String data){
  startTCPConnection();
  String response = sendRequest(buildPost(path, data));
  closeTCPConnection();
  return response;
}
String sendGet(String path){
  startTCPConnection();
  String response = sendRequest(buildGet(path));
  closeTCPConnection();
  return response;
}
